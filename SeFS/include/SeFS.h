/* $BSD_LICENSE_HEADER_START */
/*
 * Copyright 2013, 2014 Li-Yuchong(李雨翀) <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $BSD_LICENSE_HEADER_END */

/* SeFS - Simple File System, designed for realtime embedded applications.
 * It provides both tranditional functionalities of classic file systems
 * (fopen, fclose, fseek... etc.), and low-level access to storage devices
 * for high speed, realtime applications(at least it was designed to do so).
 *
 * SeFS is part of the project CyanRTOS. */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <SeFS/include/SimpleVector.h>

#define _DEBUG

#ifndef __SFS_H__
#define __SFS_H__

#define ENTRY_SIZE 4

#define END_ENTRY 0xFFFFFFFFFFFFFFFF

#define ENTRY_STATUS_AVAIL     0x00d
#define ENTRY_STATUS_OCCUPIED  0x01

#define FILE_TYPE_DIRECTORY  0x00 /* directory */
#define FILE_TYPE_FILE       0x01 /* regular file */
#define FILE_TYPE_RAW_ACCESS 0x02

#define SFS_FILENAME_LENGTH_MAX  128

struct SfsPartition
{
    struct SfsEntry *emptyEntries;
};

struct SfsEntry
{
    struct SfsEntry *nextEntry;
    
    uint8_t entryStatus;
    uint64_t dataAddr;
};

struct SfsFileHeader
{
    char fileName[SFS_FILENAME_LENGTH_MAX];
    uint8_t fileType;
    
    struct SfsEntry *firstEntry;
    uint32_t dataLength;
};


struct SfsRawAccessArea
{
    uint32_t blockOffset;
    uint32_t blockLength;
};

/* raw access to storage devices.
 * continues logic blocks will be allocated by the formating algorithm 
 * Note that the raw access entries parameters are not modificatable after
 * formating. */
struct SfsRawAccessEntry
{
    uint32_t blockOffset;
    uint32_t blockLength;
    
    uint64_t dataLength;
};

SV_DECLARE(SfsRawAccessEntries, struct SfsRawAccessEntry);

/* used in device formation */
struct SfsStorageDevice
{
    uint64_t capacity;
    
    SfsRawAccessEntries *rawAccessOption;
    struct SfsEntry *emptyEntries;
    
    uint32_t deviceBlockCount;
};

int sfsFormatDevice(struct SfsStorageDevice *device);

struct SfsFileHeader *sfsCreateFile(struct SfsStorageDevice *device,
                                    char *fileName, uint32_t entriesCount);

struct SfsFileHeader *sfsWriteFile(struct SfsFileHeader *file,
                                   struct SfsStorageDevice *device,
                                   const uint8_t *buffer,
                                   const uint64_t start, const uint64_t length);

struct SfsFileHeader *sfsReadFile(const struct SfsFileHeader *file,
                                  const struct SfsStorageDevice *device,
                                  uint8_t *buffer,
                                  const uint64_t start, const uint64_t length);

/* hardware related */
extern int
diskReadBuffer(uint8_t *buffer, struct SfsStorageDevice *device,
               const uint32_t startBlock, const uint32_t count);

extern int
diskWriteBuffer(uint8_t *buffer, struct SfsStorageDevice *device,
                const uint32_t startBlock, const uint32_t count);

/* end hardware related */

#endif /* __SFS_H__ */
