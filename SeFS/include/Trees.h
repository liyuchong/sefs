/* $BSD_LICENSE_HEADER_START */
/*
 * Copyright 2013, 2014 Li-Yuchong(李雨翀) <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $BSD_LICENSE_HEADER_END */

#ifndef __TREES_H__
#define __TREES_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>

#include <SeFS/include/SimpleVector.h>

/* AVL-Tree implementation from OpenSolaris kernel. */
#include <SeFS/include/CDDL/avl.h>

/* SimpleMap - map implementation based on AVL tree. */
typedef struct MapEntry
{
    void *key;
    void *value;
    
    avl_node_t link;
} MapEntry;

typedef avl_tree_t SimpleMap;

#define simpleMapFirst(__map) avl_first(__map)
#define simpleMapNext(__map, __node) AVL_NEXT(__map, __node)

extern SimpleMap *simpleMapInit(SimpleMap *map);
extern void simpleMapDestroy(SimpleMap *map);

extern bool simpleMapInsert(SimpleMap *map, void *key, void *value);
extern bool simpleMapDoubleInsert(SimpleMap *map, void *kv1, void *kv2);
extern void *simpleMapFind(const SimpleMap *map, void *key);

/* multi tree - provides filesystem file structure */
struct SFSMultiTreeNode;
SV_DECLARE(SFSMultiTreeNodeContainer, struct SFSMultiTreeNode *);

struct SFSMultiTreeNode
{
    char key[128];
    
    struct SFSMultiTreeNode *parent;
    SFSMultiTreeNodeContainer children;
};

/* MultiTree.c */
/** creates a tree node. */
struct SFSMultiTreeNode *sfsMultiTreeNodeCreate(const char *nodeName);

/** adds a child to a parent. returns a pointer to the child. */
struct SFSMultiTreeNode *sfsMultiTreeAddChild(struct SFSMultiTreeNode *parent,
                                              struct SFSMultiTreeNode *child);

/** remove a child from its parent. returns a pointer to the child on seccess,
 *  NULL on failure. */
struct SFSMultiTreeNode *sfsMultiTreeRemoveNode(struct SFSMultiTreeNode *node);

/** destroy a multitree */
struct SFSMultiTreeNode *sfsMultiFreeDestroy(struct SFSMultiTreeNode *root);

char *sfsMultiTreeGetDirectory(const struct SFSMultiTreeNode *node, char *buffer);
const struct SFSMultiTreeNode *sfsShowMultiTree(const struct SFSMultiTreeNode *node,
                                                uint32_t depth);

#endif /* __TREES_H__ */
