/* $BSD_LICENSE_HEADER_START */
/*
 * Copyright 2013, 2014 Li-Yuchong(李雨翀) <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $BSD_LICENSE_HEADER_END */

#include <SeFS/include/SeFS.h>

int sfsFormatDevice(struct SfsStorageDevice *device)
{
    device->emptyEntries = (struct SfsEntry *)malloc(sizeof(struct SfsEntry));
    
    device->emptyEntries->nextEntry = NULL;
    device->emptyEntries->entryStatus = ENTRY_STATUS_AVAIL;
    
    struct SfsEntry *previous = device->emptyEntries;
    
    uint64_t i;
    for(i = 0; i < device->capacity / ENTRY_SIZE ; i++)
    {
        struct SfsEntry *entry = (struct SfsEntry *)malloc(sizeof(struct SfsEntry));
        
        entry->dataAddr = (i + 1) * ENTRY_SIZE;
        entry->entryStatus = ENTRY_STATUS_AVAIL;
        
        previous->nextEntry = entry;
        previous = entry;
    }
    
    return 0;
}


struct SfsEntry *syncEntriesTable(const struct SfsEntry *entries)
{
    return (struct SfsEntry *)entries;
}

/** allocate available entries from the empty entries list.
 *  returns a pointer to the first entry of the allocated entries.
 *  returns NULL when there is no enough entris. */
struct SfsEntry *allocateEntries(struct SfsStorageDevice *device,
                                 uint32_t entriesCount)
{
    uint32_t count = 0;
    
    struct SfsEntry *entry = device->emptyEntries;
    struct SfsEntry *previous = entry;
    const struct SfsEntry *result = entry;
    
    while(entry)
    {
        if(count++ == entriesCount)
        {
            previous->nextEntry = NULL;
            device->emptyEntries = entry;
            
            break;
        }
        
        /* set entryStatus to occupied */
        entry->entryStatus = ENTRY_STATUS_OCCUPIED;
        previous = entry;
        
        if(!(entry = entry->nextEntry))
        {
            /* no enough entries */
            /* rollback, restore entryStatus to avaliable status */
            entry = device->emptyEntries;
            while(entry != NULL)
            {
                entry->entryStatus = ENTRY_STATUS_AVAIL;
                entry = entry->nextEntry;
            }
            
            return NULL;
        }
    }
    
    return syncEntriesTable(result);
}

/** deallocate allocated entries. returns a pointer to the first entry of the
 *  available entries. */
struct SfsEntry *deallocateEntries(struct SfsStorageDevice *device,
                                   struct SfsEntry *firstEntry)
{
    struct SfsEntry *entry = firstEntry;
    struct SfsEntry *lastEntry;
    
    syncEntriesTable(firstEntry);
    
    while(entry)
    {
        entry->entryStatus = ENTRY_STATUS_AVAIL;
        lastEntry = entry;
        
        entry = entry->nextEntry;
    }
    
    struct SfsEntry *first = device->emptyEntries;
    device->emptyEntries = firstEntry;
    lastEntry->nextEntry = first;
    
    return device->emptyEntries;
}

struct SfsFileHeader *sfsCreateFile(struct SfsStorageDevice *device,
                                    char *fileName, uint32_t entriesCount)
{
    struct SfsFileHeader *file =
    (struct SfsFileHeader *)malloc(sizeof(struct SfsFileHeader));
    
    strcpy(file->fileName, fileName);
    
    file->fileType = FILE_TYPE_FILE;
    file->firstEntry = allocateEntries(device, entriesCount);
    file->dataLength = 0;
    
    return file;
}

struct SfsEntry *getKthEntry(struct SfsEntry *first, uint32_t k)
{
    struct SfsEntry *entry = first;
    struct SfsEntry *result = NULL;
    
    uint32_t entryCount = 0;
    while(entry)
    {
        result = entry;
        entry = entry->nextEntry;
        
        if(entryCount++ == k)
        {
            assert(result);
            
            return result;
            break;
        }
    }
    
    return NULL;
}

/* a lazy macro to move entryToWrite to next node and
 * re-calculate logicAddress. */
#define __MOVE_TO_NEXT_ENTRY_RECALC_LOGIC_ADDR(__e)                      \
do {                                                                \
(__e) = (__e)->nextEntry;                         \
assert(__e);                                           \
\
logicAddress = (uint32_t)((__e)->dataAddr / ENTRY_SIZE); \
} while(0)

struct SfsFileHeader *
sfsWriteFile(struct SfsFileHeader *file, struct SfsStorageDevice *device,
             const uint8_t *buffer,
             const uint64_t start, const uint64_t length)
{
    /* first, find the length of the allocated entries of the file */
    uint64_t allocatedEntries = 0;
    
    struct SfsEntry *entry = file->firstEntry;
    struct SfsEntry *lastEntry = entry;
    
    while(entry)
    {
        lastEntry = entry;
        entry = entry->nextEntry;
        
        allocatedEntries++;
    }
    
    assert(lastEntry);
    
    /* calculate the total capacity and the available capacity */
    uint64_t totalCapacity     = allocatedEntries * ENTRY_SIZE;
    uint64_t availableCapacity = totalCapacity - start - 1;
    
    /* calculate bytes need to be allocated */
    uint64_t bytesToAllocate = 0;
    if(length > availableCapacity) bytesToAllocate = length - availableCapacity;
    
    /* calculate entries need to be allocated */
    uint32_t entriesToAllocate = 0;
    if(bytesToAllocate > 0)
    {
        entriesToAllocate = (uint32_t)((bytesToAllocate) / ENTRY_SIZE + 1);
        
        assert(lastEntry->nextEntry == NULL);
        lastEntry->nextEntry = allocateEntries(device, entriesToAllocate);
        
        if(lastEntry->nextEntry == NULL) /* fail to allocate new entries */
        {
#ifdef _DEBUG
            fprintf(stderr, "error: unable to allocate new entries\n");
#endif
            return NULL;
        }
    }
    
#ifdef _DEBUG
    fprintf(stderr, "\nfile: %s\n", file->fileName);
    fprintf(stderr, "%llu entries allocated, total capacity %llu bytes.\n",
            allocatedEntries, totalCapacity);
    fprintf(stderr, "available: %llu, bytes to allocate: %llu, "
            "entries to allocate: %u\n",
            availableCapacity, bytesToAllocate, entriesToAllocate);
#endif
    
    uint8_t *deviceBuffer =
    (uint8_t *)malloc(device->deviceBlockCount * ENTRY_SIZE);
    
    /* initialize: find the first entry to be written */
    struct SfsEntry *entryToWrite =
        getKthEntry(file->firstEntry, (uint32_t)(start / ENTRY_SIZE));
    assert(entryToWrite);
    
    /* logicAddress is the position of the entry->dataAddr
     * in the storage device. */
    uint32_t logicAddress;
    
    /* bytesToWrite is the count of bytes should be written to the device */
    register uint64_t bytesToWrite = length;
    
    /* eg.: bytesInEntry = 3, bytesLeftEntry = 1
     * in the following graph: (^ indicates dataLength)
     *
     *  x x x x
     *  x x ^ .
     *  . . . .
     */
    const uint32_t bytesInEntry = start % ENTRY_SIZE;
    const uint32_t bytesLeftEntry = ENTRY_SIZE - bytesInEntry;
    
    /* handle the current entry */
    logicAddress = (uint32_t)(entryToWrite->dataAddr / ENTRY_SIZE);
    
    /* we can't modify single byte directly since the devices are block-based.
     * thus we need to read data from the device first if the length of data
     * we want to write to the device is less than 
     * the length of the device block */
    diskReadBuffer(deviceBuffer, device, logicAddress, device->deviceBlockCount);
    
    /* the length of data need to be written is less than the left space in 
     * the current entry */
    if(bytesToWrite < bytesLeftEntry)
    {
        memcpy(deviceBuffer + bytesInEntry, buffer, bytesToWrite);
        bytesToWrite = 0;
    }
    else
    {
        memcpy(deviceBuffer + bytesInEntry, buffer, bytesLeftEntry);
        bytesToWrite -= bytesLeftEntry;
    }
    
    diskWriteBuffer(deviceBuffer, device,
                    logicAddress, device->deviceBlockCount);

    /* handle the middle entries(full ENTRY_SIZE entries)
     * write ENTRY_SIZE bytes in each loop */
    register uint32_t count = 0;
    for(; bytesToWrite >= ENTRY_SIZE + 1; bytesToWrite -= ENTRY_SIZE)
    {
        __MOVE_TO_NEXT_ENTRY_RECALC_LOGIC_ADDR(entryToWrite);
        
        /* no need to read data from the device, 
         * just overlap the original data. */
        memcpy(deviceBuffer,
               buffer + ++count * ENTRY_SIZE - bytesInEntry,
               ENTRY_SIZE);
        
        diskWriteBuffer(deviceBuffer, device, logicAddress,
                        device->deviceBlockCount);
    }
    
    /* handl the final entry */
    __MOVE_TO_NEXT_ENTRY_RECALC_LOGIC_ADDR(entryToWrite);
    
    diskReadBuffer(deviceBuffer, device, logicAddress, device->deviceBlockCount);
    memcpy(deviceBuffer, buffer + length - bytesToWrite, bytesToWrite);
    diskWriteBuffer(deviceBuffer, device, logicAddress, device->deviceBlockCount);
    
    /* done, modify data length */
    file->dataLength += length;
    
#ifdef _DEBUG
    fprintf(stderr, "%llu bytes written, current file data length: %d\n",
                    length, file->dataLength);
    fprintf(stderr, "\n");
#endif
    
    /* TODO: add from? */
    /* TODO: check if there are extra entries, deallocate them. */

    free(deviceBuffer);
    deviceBuffer = NULL;
    
    return file;
}

struct SfsFileHeader *sfsReadFile(const struct SfsFileHeader *file,
                                  const struct SfsStorageDevice *device,
                                  uint8_t *buffer,
                                  const uint64_t start, const uint64_t length)
{
    /* first locate the first entry to read from */
    struct SfsEntry *entryToRead =
        getKthEntry(file->firstEntry, (uint32_t)(start / ENTRY_SIZE));
    assert(entryToRead);
    
    const uint32_t bytesInEntry = start % ENTRY_SIZE;
    const uint32_t bytesLeftEntry = ENTRY_SIZE - bytesInEntry;
    
    uint32_t logicAddress;
    uint8_t *deviceBuffer =
        (uint8_t *)malloc(device->deviceBlockCount * ENTRY_SIZE);

    uint64_t bytesToRead = length;
    const uint64_t maxLengthToRead = file->dataLength - start;
    if(bytesToRead > maxLengthToRead) bytesToRead = maxLengthToRead;
    uint64_t len = bytesToRead;
    
    /* read the first entry */
    logicAddress = (uint32_t)(entryToRead->dataAddr / ENTRY_SIZE);
    diskReadBuffer(deviceBuffer, (struct SfsStorageDevice *)device,
                   logicAddress, device->deviceBlockCount);
    
    if(bytesToRead < bytesLeftEntry)
    {
        memcpy(buffer, deviceBuffer + bytesInEntry, bytesToRead);
        bytesToRead = 0;
    }
    else
    {
        memcpy(buffer, deviceBuffer + bytesInEntry, bytesLeftEntry);
        bytesToRead -= bytesLeftEntry;
    }
    
    /* middle entries */
    register uint32_t count = 0;
    for(; bytesToRead >= ENTRY_SIZE + 1; bytesToRead -= ENTRY_SIZE)
    {
        __MOVE_TO_NEXT_ENTRY_RECALC_LOGIC_ADDR(entryToRead);
        
        diskReadBuffer(deviceBuffer, (struct SfsStorageDevice *)device,
                       logicAddress, device->deviceBlockCount);

        memcpy(buffer + ++count * ENTRY_SIZE - bytesInEntry,
               deviceBuffer ,
               ENTRY_SIZE);
    }
    
    /* the final entry */
    __MOVE_TO_NEXT_ENTRY_RECALC_LOGIC_ADDR(entryToRead);
    
    diskReadBuffer(deviceBuffer, (struct SfsStorageDevice *)device,
                   logicAddress, device->deviceBlockCount);
    memcpy(buffer + len - bytesToRead, deviceBuffer, bytesToRead);
    
    /* done, clean up */
    
    free(deviceBuffer);
    deviceBuffer = NULL;
    
    return (struct SfsFileHeader *)file;
}

#undef __MOVE_TO_NEXT_ENTRY_RECALC_LOGIC_ADDR
