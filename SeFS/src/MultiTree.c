/* $BSD_LICENSE_HEADER_START */
/*
 * Copyright 2013, 2014 Li-Yuchong(李雨翀) <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $BSD_LICENSE_HEADER_END */

#include <SeFS/include/Trees.h>

struct SFSMultiTreeNode *sfsMultiTreeNodeCreate(const char *nodeName)
{
    struct SFSMultiTreeNode *node
        = (struct SFSMultiTreeNode *)malloc(sizeof(struct SFSMultiTreeNode));
    strcpy(node->key, nodeName);
    
    SV_INIT(&node->children, 2, struct SFSMultiTreeNode *);
    
    return node;
}

struct SFSMultiTreeNode *sfsMultiTreeAddChild(struct SFSMultiTreeNode *parent,
                                              struct SFSMultiTreeNode *child)
{
    SV_PUSH_BACK(&parent->children, child, struct SFSMultiTreeNode *);
    child->parent = parent;
    
    return child;
}

struct SFSMultiTreeNode *sfsMultiTreeRemoveNode(struct SFSMultiTreeNode *node)
{
    assert(node);
    
    if(!node->parent) /* no parent, just return */
        return node;
    
    struct SFSMultiTreeNode *parent = node->parent;
    size_t i;
    for(i = 0 ;i < SV_SIZE(&parent->children); i++) /* locate child */
    {
        if(SV_AT(&parent->children, i) == node)
        {
            /* found, erase it */
            SV_ERASE_AT(&parent->children, i, struct SFSMultiTreeNode *);
            node->parent = NULL;
            
            return node;
        }
    }
    
    /* unable to find child, error */
    return NULL;
}

struct SFSMultiTreeNode *sfsMultiFreeDestroy(struct SFSMultiTreeNode *root)
{
    size_t i;
    for(i = 0; i < SV_SIZE(&root->children); i++)
    {
        struct SFSMultiTreeNode *child = SV_AT(&root->children, i);
        sfsMultiFreeDestroy(child);
    }
    
    free(root->children.data);
    root->children.data = NULL;
    free(root);
    root = NULL;
    
    return root;
}

char *sfsMultiTreeGetDirectory(const struct SFSMultiTreeNode *node, char *buffer)
{
    struct SFSMultiTreeNode *nd = (struct SFSMultiTreeNode *)node;
    
    SFSMultiTreeNodeContainer directory;
    SV_INIT(&directory, 2, struct SFSMultiTreeNode *);
    
    while(nd)
    {
        SV_PUSH_BACK(&directory, nd, struct SFSMultiTreeNode *);
        nd = nd->parent;
    }
    
    buffer[0] = '\0';
    size_t i;
    for(i = SV_SIZE(&directory) - 1; i != 0; i--)
    {
        strcat(buffer, SV_AT(&directory, i)->key);
        strcat(buffer, "/");
        
    }
    strcat(buffer, SV_AT(&directory, 0)->key);
    
    free(directory.data);
    
    return buffer;
}

const struct SFSMultiTreeNode *sfsShowMultiTree(const struct SFSMultiTreeNode *node,
                                                uint32_t depth)
{
    if(depth == 0)
        fprintf(stdout, "%s\n", node->key);
    
    depth++;
    
    size_t i, j;
    for(i = 0; i < SV_SIZE(&node->children); i++)
    {
        struct SFSMultiTreeNode *child = SV_AT(&node->children, i);
        
        for(j = 0; j < depth - 1; j++) printf("|  ");
        
        if(i == SV_SIZE(&node->children) - 1)
            fprintf(stdout, "└──");
        else
            fprintf(stdout, "├──");
        
        fprintf(stdout, "%s\n", child->key);
        
        sfsShowMultiTree(child, depth);
    }
    
    return node;
}
