/* $BSD_LICENSE_HEADER_START */
/*
 * Copyright 2013, 2014 Li-Yuchong(李雨翀) <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* $BSD_LICENSE_HEADER_END */

/*
 *  SimpleMap.c
 *
 *  Created by Li-Yuchong <liyuchong@cyanclone.com> on 13-9-4.
 *  Copyright (c) 2013 Cyanclone Co., Ltd.. All rights reserved.
 */

/* map implementation based on AVL tree. */

#include <SeFS/include/Trees.h>

static int avlCompareFunc(const void *a, const void *b)
{
#define _CAST(__var, __type) ((__type)(__var))

    if(_CAST(a, MapEntry *)->key < _CAST(b, MapEntry *)->key)
        return -1;
    else if (_CAST(a, MapEntry *)->key > _CAST(b, MapEntry *)->key)
        return 1;
    else
        return 0;
    
#undef _CAST
}

SimpleMap *simpleMapInit(SimpleMap *map)
{
    avl_create(map, avlCompareFunc, sizeof(SimpleMap), offsetof(MapEntry, link));
    
    return map;
}

void simpleMapDestroy(SimpleMap *map)
{
    void *cookie = NULL;
    MapEntry *node;
  
    while((node = (MapEntry *)avl_destroy_nodes(map, &cookie)))
        free(node);
  
    avl_destroy(map);
    
    free(map);
}

bool simpleMapInsert(SimpleMap *map, void *key, void *value)
{
    MapEntry *node = (MapEntry *)malloc(sizeof(MapEntry));
    node->key = key;
    node->value = value;
    
    avl_add(map, node);
    
    return true;
}

bool simpleMapDoubleInsert(SimpleMap *map, void *kv1, void *kv2)
{
    return simpleMapInsert(map, kv1, kv2) && simpleMapInsert(map, kv2, kv1);
}

void *simpleMapFind(const SimpleMap *map, void *key)
{
    MapEntry search;
    search.key = key;

    MapEntry *result = avl_find((SimpleMap *)map, &search, NULL);
    
    return result ? result->value : NULL;
}
